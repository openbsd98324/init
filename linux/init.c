

// 
// The Mini Barebone Linux
// ROOTFS: it requires only the vmlinuz started from lilo, /sbin/init and /dev/ /proc/ /sys/ only. 


// 
// For /sbin/init: 
//     clang -static init.c -o init   
//
// Thanks to dave0 !
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <fcntl.h>
#define PAMAX 2046 

//#include <stdlib.h> 
//#include <string.h>
#include <dirent.h>
//#include <ctype.h>
//#include <sys/stat.h>
//#include <sys/types.h>
//#include <unistd.h> 
//#include <time.h>

static sigset_t set;
/*
   The key is to use tcgetattr(ttyfd, &attributes) on the current terminal's file descriptor to retrieve its current attributes into in a struct termios, edit the attributes, then apply the changes with tcsetattr(ttyfd, when, &attributes).
 */



void nls()
{ 
	DIR *dirp;
	struct dirent *dp;
	dirp = opendir( "." );
	while  ((dp = readdir( dirp )) != NULL ) 
	{
		if (  strcmp( dp->d_name, "." ) != 0 )
			if (  strcmp( dp->d_name, ".." ) != 0 )
				printf( "%s\n", dp->d_name );
	}
	closedir( dirp );
}




int main(int argc, char *argv[])
{

        char foocharo[PAMAX];

	char   buf[80];
	int    numBytes;
	struct termios original, tattered;
	int    ttyfd;

	/* Open the controlling terminal. */
	//ttyfd = open( "/dev/tty1" , O_RDWR);

    ttyfd = STDIN_FILENO ; 

	if(ttyfd < 0){
		printf("Could not open tty!\n");
		//return -1;
	}

	/**
	 * Get current terminal properties, save them (including current KILL char),
	 * set the new KILL char, and make this the new setting.
	 */

	tcgetattr(ttyfd, &original);
	tattered = original;
	tattered.c_cc[VKILL] = 033;/* New killchar, 033 == ESC. */
	tcsetattr(ttyfd, TCSANOW, &tattered);

	/**
	 * Simple test to see whether it works.
	 */

	write(1, "Please enter a line: ", 21);
	numBytes = read(0, buf, sizeof buf);
	write(1, buf, numBytes);


	printf ("The process ID is %d\n", (int) getpid ());
	printf ("The parent process ID is %d\n", (int) getppid ());

	chdir("/");
	sigfillset( &set );
	sigprocmask( SIG_BLOCK, &set, NULL);

	int counter = 0 ; 
	while( counter <= 2 )  
	{
		printf( " Hello world!\n");
		printf( " gcc -static hello.c -o init "); 
		printf( " echo init | cpio -o -H newc | gzip > test.cpio.gz " ) ; 
		printf( " qemu -kernel /boot/vmlinuz -initrd test.cpio.gz /dev/zero " ); 
		//sleep(999999999);
		int ch ; 
		//ch = getchar(); 
		counter++;
	}

	char var_lastcommand[PAMAX];
	char target[PAMAX];

	fprintf( stderr, "Welcome to Init (type bsh, for minimal shell.)\n" ); 
	fprintf( stderr, " (Esc, 27) to quit.\n" ); 
	char commandprompt[PAMAX];
	char charo[PAMAX];
	char cwd[PAMAX];
	strncpy( commandprompt, "", PAMAX );

	struct termios ot;
	if(tcgetattr(STDIN_FILENO, &ot) == -1) perror(")-");
	struct termios t = ot;
	t.c_lflag &= ~(ECHO | ICANON);
	t.c_cc[VMIN] = 1;
	t.c_cc[VTIME] = 0;
	if(tcsetattr(STDIN_FILENO, TCSANOW, &t) == -1) perror(")-");

	int a = 0;
	int gameover = 0; 
	int cmdlen = 0; 
	int debug = 0; 

	while( gameover == 0 ) 
	{
		//fprintf( stderr, "you pressed '%c' (d %d)\n", a, a );
		fprintf( stderr, "A:> %s\n", commandprompt ); 
		a = getchar(); 
		if ( a == 27 ) gameover = 1 ; 

		else if  ( a == 127 )
		{
			strncpy( commandprompt, "" , PAMAX);
		}

		else if ( ( a == 13 ) || ( a == 10 ) )
		{
			if ( debug == 1 )
			{
				printf( "Char  %d \n" , a );   // 10 is enter
				printf( "Strln %d \n" , strlen( commandprompt ) );   // 10 is enter
			}

			cmdlen = strlen( commandprompt );



			if ( ( commandprompt[0]    == 'c'  ) 
					&& ( commandprompt[1]  == 'd'  ) 
					&& ( commandprompt[2]  == ' '  ) ) 
			{
				strcpy( target, commandprompt +3 );
				fprintf( stderr, "Strip %s\n", target ); 
				chdir(  target ); 
				printf( "===\n" );
				printf( "PATH: %s\n", getcwd( cwd , PAMAX ) );
				printf( "===\n" );
				strncpy( commandprompt, "" , PAMAX);
			}






			else if ( ( commandprompt[0]    == 'n'  )
					&& ( commandprompt[1]  == 'l'  )   
					&& ( commandprompt[2]  == 's'  )
					&& ( cmdlen == 3 ) ) 
			{
				printf( "===\n" );
				printf( "PATH: %s\n", getcwd( cwd , PAMAX ) );
				printf( "===\n" );
				nls(); 
				strncpy( commandprompt, "" , PAMAX);
			}





			else if ( ( commandprompt[0]    == 'p'  )
					&& ( commandprompt[1]  == 'w'  )   
					&& ( commandprompt[2]  == 'd'  )
					&& ( cmdlen == 3 ) ) 
			{
				printf( "===\n" );
				printf( "PATH: %s\n", getcwd( cwd , PAMAX ) );
				printf( "===\n" );
				strncpy( commandprompt, "" , PAMAX);
			}


			else if ( ( commandprompt[0]    == '!'  )
					&& ( commandprompt[1]  == 'h'  )   
					&& ( commandprompt[2]  == 'e'  )   
					&& ( commandprompt[3]  == 'l'  )   
					&& ( commandprompt[4]  == 'p'  )   
					&& ( cmdlen == 4+1 ) ) 
			{
				fprintf( stderr, "==================\n" ); 
				fprintf( stderr, "=== soft: INIT ===\n" ); 
				fprintf( stderr, "==================\n" ); 
				fprintf( stderr, "=== !HELP ===\n" ); 
				fprintf( stderr, "\n" ); 
				fprintf( stderr, "pwd:   shows the current path.\n" ); 
				fprintf( stderr, "cd:    change current directory.\n" ); 
				fprintf( stderr, "\n" ); 
				fprintf( stderr, "!last: shows the last command.\n" ); 
				fprintf( stderr, "!time: gives the unix epoch time.\n" ); 
				fprintf( stderr, "\n" ); 
				strncpy( commandprompt, "" , PAMAX);
			}




			else if ( ( commandprompt[0]    == '!'  )
					&& ( commandprompt[1]  == 't'  )   
					&& ( commandprompt[2]  == 'i'  )   
					&& ( commandprompt[3]  == 'm'  )   
					&& ( commandprompt[4]  == 'e'  )   
					&& ( cmdlen == 4+1 ) ) 
			{
				fprintf( stderr , "%d\n", (int)time(NULL));
			        printf( "%d\n", (int)time(NULL));
				strncpy( commandprompt, "" , PAMAX);
			}





			else if ( ( commandprompt[0]    == '!'  )
					&& ( commandprompt[1]  == 'q'  )   
					&& ( commandprompt[2]  == 'u'  )   
					&& ( commandprompt[3]  == 'i'  )   
					&& ( commandprompt[4]  == 't'  )   
					&& ( cmdlen == 4+1 ) ) 
			{
				printf( " ! INIT TERMINATED !\n" ); 
				strncpy( commandprompt, "" , PAMAX);
				gameover = 1; 
			}




			else if ( ( commandprompt[0]    == '!'  )
					&& ( commandprompt[1]  == 'l'  )   
					&& ( commandprompt[2]  == 'a'  )   
					&& ( commandprompt[3]  == 's'  )   
					&& ( commandprompt[4]  == 't'  )   
					&& ( cmdlen == 4+1 ) ) 
			{
				printf( "===\n" );
				fprintf( stderr, "Last command:\n" ); 
				fprintf( stderr, "  %s\n", var_lastcommand ); 
				printf( "===\n" );
				strncpy( commandprompt, "" , PAMAX);
			}



			else if ( ( commandprompt[0]    == 'c'  ) && ( commandprompt[1]  == 'd'  )   
					&& ( cmdlen == 1+1 ) ) 
			{
				printf( "HOME: %s\n", getenv( "HOME" ));
				chdir(  getenv( "HOME" ) ); 
				printf( "===\n" );
				printf( "PATH: %s\n", getcwd( cwd , PAMAX ) );
				printf( "===\n" );
				strncpy( commandprompt, "" , PAMAX);
			}






			else if ( ( commandprompt[0]    == 't'  ) 
					&& ( commandprompt[1]  == 's'  )   
					&& ( commandprompt[2]  == 'h'  ) 
					&& ( cmdlen == 2+1 ) ) 
			{
				strcpy( target, commandprompt +3 );
				fprintf( stderr, "Strip %s\n", target ); 
				printf( "===\n" );
				printf( "PATH: %s\n", getcwd( cwd , PAMAX ) );
				printf( "===\n" );
				printf( "CMD: %s\n" , foocharo ); 
                                execlp( "tsh", "tsh", "-f", "nls", 0);
				// cleanup
				strncpy( commandprompt, "" , PAMAX);
			}



			else if ( ( commandprompt[0]           == 'b'  ) 
					&& ( commandprompt[1]  == 's'  )   
					&& ( commandprompt[2]  == 'h'  ) 
					&& ( cmdlen == 2+1 ) ) 
			{
				strcpy( target, commandprompt +3 );
				fprintf( stderr, "Strip %s\n", target ); 
				printf( "===\n" );
				printf( "PATH: %s\n", getcwd( cwd , PAMAX ) );
				printf( "===\n" );
				printf( "CMD: %s\n" , foocharo ); 
                                execlp( "bsh", "bsh", "-f", "nls", 0);
				// cleanup
				strncpy( commandprompt, "" , PAMAX);
			}



			else
			{
				fprintf( stderr, "Run\n" ); 
				strncpy( var_lastcommand, commandprompt , PAMAX);
				system( commandprompt );
				// cleanup
				strncpy( commandprompt, "" , PAMAX);
			}
		}

		else
		{
			snprintf( charo, sizeof(charo), "%s%c", commandprompt, a );
			strncpy( commandprompt, charo , PAMAX);
		}
	}

	if(tcsetattr(STDIN_FILENO, TCSANOW, &ot) == -1) perror(")-");
	/**
	 * Restore original settings.
	 */
	tcsetattr(ttyfd, TCSANOW, &original);

	/* Clean up. */
	close(ttyfd);

	return 0; 
}







/*
   cat > hello.c << EOF
#include <stdio.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
printf("Hello world!\n");
sleep(999999999);
}
EOF
gcc -static hello.c -o init
echo init | cpio -o -H newc | gzip > test.cpio.gz
# Testing external initramfs using the initrd loading mechanism.
qemu -kernel /boot/vmlinuz -initrd test.cpio.gz /dev/zero

#include <stdio.h> 
#include <unistd.h>
#include <fcntl.h> 
int main() {                                                                                                                                                  
char byte;                                                                                                                                                
int fd = open("/dev/pts/3", O_RDWR);                                                                                                                      
write(fd, "X", 1);                                                                                                                                        
ssize_t size = read(fd, &byte, 1);                                                                                                                        
printf("Read byte %c\n", byte);                                                                                                                           
return 0;                                                                                                                                                 
}
*/




